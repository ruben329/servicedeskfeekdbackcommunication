﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace ServiceDeskFeedbackConfirmation.API
{
    public class ApiResponse
    {
        public bool HasErrors { get; set; }
        public string ErrorMessage { get; set; }
        public string Result { get; set; }
        public ApiResponse(WebResponse response)
        {

            HasErrors = false;
            ErrorMessage = string.Empty;
            Result = string.Empty;
            StreamReader reader = new StreamReader(response.GetResponseStream());
            Result = reader.ReadToEnd();
            if (Result == "null")
                Result = null;

        }
        public ApiResponse()
        {
            HasErrors = false;
            ErrorMessage = string.Empty;
            Result = string.Empty;
        }

    }
    public enum APIResourceVerb
    {
        PUT,
        POST,
        GET,
        DELETE

    }
}
