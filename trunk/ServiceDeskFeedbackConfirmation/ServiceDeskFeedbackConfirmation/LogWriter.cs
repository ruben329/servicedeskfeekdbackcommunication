using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Web.Hosting;


public class LogWriter : TraceListener
{
    private static Boolean _keepRuning;

    private static Thread _thread;

    private static readonly Object Locker = new Object();

    private static readonly List<LogItem> Messages = new List<LogItem>();

    // ReSharper disable UnusedMember.Global
    public static void WriteLog(string fileName, string textLine)
    // ReSharper restore UnusedMember.Global
    {
        TextWriter tw = null;

        try
        {
            var fi = new FileInfo(fileName);

            if (!fi.Exists)
#pragma warning disable 168
                using (FileStream fs = fi.Create())
#pragma warning restore 168
                {
                }

            using (tw = new StreamWriter(fileName, true, System.Text.Encoding.Default))
            {
                tw.WriteLine("[" + DateTime.Now + "] " + textLine);
                tw.Flush();
            }
        }
        // ReSharper disable EmptyGeneralCatchClause
        catch (Exception e)
        // ReSharper restore EmptyGeneralCatchClause
        {
            //do nothing
        }
        finally
        {
            if (tw != null) tw.Close();
        }
    }

    public override void Write(string logFile, string message)
    {
        Write(logFile, message, "");
    }

    public void DeleteFile(string logFile)
    {
        DeleteTheFile(logFile);
    }

    public override void Write(string message)
    {
        string logFile = ConfigurationManager.AppSettings["JiraLogPath"].ToString() + "\\" + DateTime.Now.Year + "-" + DateTime.Now.DayOfYear +
                         ".txt";
        Write(logFile, message, "");
    }

    public override void WriteLine(string message)
    {
        Write(message);
    }

    public override void Write(object o)
    {
        string logFile = ConfigurationManager.AppSettings["JiraLogPath"].ToString() + "\\" + DateTime.Now.Year + "-" + DateTime.Now.DayOfYear +
                         ".txt";

        if (o is Exception)
        {
            var ex = (Exception)o;
            Write(logFile, ex.Message, ex.StackTrace);
            Trace.WriteLine(o);
        }
    }

    public override void WriteLine(object o)
    {
        Write(o);
    }

    private static void Write(string fileName, string message, string source)
    {
        if (_thread == null)
            Start();

        lock (Locker)
        {
            Messages.Add(new LogItem(fileName, message, source));
            Monitor.Pulse(Locker);
        }
    }

    private static void DeleteTheFile(string fileName)
    {
        Write(fileName, "DELETE", "DELETE");
    }

    private static void Start()
    {
        _keepRuning = true;
        _thread = new Thread(Run);
        _thread.Start();
    }

    private static void Run()
    {
        do
        {
            try
            {
                lock (Locker)
                {
                    if (_keepRuning)
                        Monitor.Wait(Locker);

                    if (Messages.Count > 0)
                    {
                        String fileToDelete = "";
                        foreach (LogItem message in Messages)
                        {
                            if (message.Message == "DELETE" && message.Source == "DELETE")
                            {
                                fileToDelete = message.FileName;
                                File.Delete(message.FileName);
                                continue;
                            }

                            if (fileToDelete == message.FileName)
                                continue;

                            // ReSharper disable AssignNullToNotNullAttribute
                            using (var tw = new StreamWriter(message.FileName, true, System.Text.Encoding.Default))
                            // ReSharper restore AssignNullToNotNullAttribute
                            {
                                if (message.Source != "")
                                {
                                    tw.WriteLine("[" + DateTime.Now.ToString("HH:mm::ss") + "] - " + message.Message);
                                    tw.WriteLine("[Stack]     - " + message.Source);
                                }
                                else
                                    tw.WriteLine("[" + DateTime.Now.ToString("HH:mm::ss") + "] - " + message.Message);

                                tw.Flush();
                            }
                        }

                        Messages.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        } while (_keepRuning);
    }

    public new void Dispose()
    {
        lock (Locker)
        {
            _keepRuning = false;
            Monitor.Pulse(Locker);
        }

        base.Dispose();
    }

    #region Nested type: LogItem

    private class LogItem
    {
        public readonly String FileName;
        public readonly String Message;
        public readonly String Source;

        public LogItem(string file, string message, string source)
        {
            FileName = file;
            Message = message;
            Source = source;
        }
    }

    #endregion
}