﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Security.Cryptography;
using System.Text;
using System.Configuration;

namespace ServiceDeskFeedbackConfirmation
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            if (!Page.IsPostBack)
            {
                string variables = Request.ServerVariables["ALL_RAW"];
                LogToFil.Log("defaultlog", "\r\n-------------------------\r\nRequest: " + Request.QueryString);
                LogToFil.Log("defaultlog", "Headers: " + variables);
                LogToFil.Log("defaultlog", "HTTP_REFERER: " + Request.ServerVariables["HTTP_REFERER"]);
                LogToFil.Log("defaultlog", "LOCAL_ADDR: " + Request.ServerVariables["LOCAL_ADDR"]);
                LogToFil.Log("defaultlog", "REMOTE_ADDR: " + Request.ServerVariables["REMOTE_ADDR"]);
                LogToFil.Log("defaultlog", "REMOTE_HOST: " + Request.ServerVariables["REMOTE_HOST"]);
                LogToFil.Log("defaultlog", "SERVER_NAME: " + Request.ServerVariables["SERVER_NAME"]);
               
                string hldid = Request.QueryString["hldid"];
                string score = Request.QueryString["score"];
                string email = Request.QueryString["param"];
                string key = Request.QueryString["key"];
                if (hldid == null )
                {
                    LogToFil.Log("defaultlog", "hldid missing from query, returning");
                    return;
                }
                if( score == null )
                {
                    LogToFil.Log("defaultlog", "score missing from query, returning");
                    return;
                }
                if ( email == null )
                {
                    LogToFil.Log("defaultlog", "email missing from query, returning");
                    return;
                }
                if ( key == null )
                {
                    LogToFil.Log("defaultlog", "key missing from query, returning");
                    return;
                }
                int intscore;
                hldid = hldid.Trim();
                email = email.Trim();
                score = score.Trim();
                key = key.Trim();

                if (!string.IsNullOrEmpty(hldid) && hldid.Length > 6)
                {
                    hldid = hldid.Substring(0, 6);
                }
                
                //Validate input against key element
                using (MD5 md5Hash = MD5.Create())
                {
                    string md5key = GetMd5Hash(md5Hash, Request.QueryString["hldid"].Trim() + "-" + Request.QueryString["param"].Trim()).ToUpper();
                    StringComparer comparer = StringComparer.OrdinalIgnoreCase;
                    if ( comparer.Compare(md5key, key) != 0)
                    {
                        string log = string.Format("Key was not the same, returning. urlkey={0} <> md5key={1}", key, md5key);
                        LogToFil.Log("defaultlog", log);

                        System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage("no-reply@hatteland.com", "ruben.olsen@hatteland.com", "Vi har fått inn en bogus stemme", log + "\r\nEmail: " + Request.QueryString["param"].Trim() + "\r\nhldid: " + Request.QueryString["hldid"].Trim());
                        mailMessage.IsBodyHtml = true;
                        Attachment att = new Attachment(ConfigurationManager.AppSettings["LogPath"].ToString() + "\\HLDdefaultlog.log");
                        mailMessage.Attachments.Add(att);
                        SmtpClient smtpClient = GetSmtpClient();
                        smtpClient.Send(mailMessage);
                        return;
                    }
                }
                if (!string.IsNullOrEmpty(hldid) && !string.IsNullOrEmpty(score) && int.TryParse(score, out intscore))
                {
                    try
                    {
                        LogToFil.Log(hldid, "Score: " + score);
                        RateData rate = new RateData(intscore);

                        RambaseLink link = new RambaseLink(hldid);
                        API.ApiResponse resp = link.RunQuery(string.Format("collaboration/service-desk-requests/{0}/support-ratings", hldid), null, rate, API.APIResourceVerb.POST, false);
                        LogToFil.Log(hldid, resp.Result);
                        if (score == "1")
                        {
                            // Send email to Servicedesk manager
                            SendEmailToAdmin(string.Format("User({0}) was not satisfied (ratescore:{1}) with how we solved ticket HLD/{2}.", email, score, hldid));
                        }
                        LogToFil.Log(hldid, "Finishing...." + score);
                    }
                    catch (Exception ex)
                    {
                        LogToFil.Log(hldid, ex.Message);
                        Response.Write(ex.Message);
                    }
                    finally
                    {
                        LogToFil.Log(hldid, "-------------------------------------------" + score);
                    }

                }

            }
        }
        private static void SendEmailToAdmin(string body)
        {
            string emailTo = System.Configuration.ConfigurationManager.AppSettings["AdminEmailAddress"];
            System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage("no-reply@hatteland.com", emailTo, "User was not satisfied", body + "\r\n\r\nPlease follow up this customer.");
            mailMessage.IsBodyHtml = true;

            SmtpClient smtpClient = GetSmtpClient();

            try
            {
                smtpClient.Send(mailMessage);

            }
            catch
            {
                //  error = string.Format("Failed to send email change email: {0}", ex.InnerException);
            }

        }
        public static SmtpClient GetSmtpClient()
        {
            var smtpServerHost = System.Configuration.ConfigurationManager.AppSettings["SmtpServer"];
            var smtpServerUsername = System.Configuration.ConfigurationManager.AppSettings["SmtpServerUsername"];
            var smtpServerPassword = System.Configuration.ConfigurationManager.AppSettings["SmtpServerPassword"];

            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Host = smtpServerHost;
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new System.Net.NetworkCredential(smtpServerUsername, smtpServerPassword);
            //        smtpClient.Port = 587;
            smtpClient.EnableSsl = true;

            return smtpClient;
        }

        private string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash. 

            // Untested
            //byte[] data = md5Hash.ComputeHash(Encoding.GetEncoding(1252).GetBytes(input));
            byte[] data = md5Hash.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes 
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data  
            // and format each one as a hexadecimal string. 
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string. 
            return sBuilder.ToString();
        }
    }


}