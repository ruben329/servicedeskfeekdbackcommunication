﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ServiceDeskFeedbackConfirmation.API;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Caching;

namespace ServiceDeskFeedbackConfirmation
{
    public class RambaseLink
    {
        
        private string ClientId = ConfigurationManager.AppSettings["RambaseClientId"].ToString();
        private string ClientSecret = ConfigurationManager.AppSettings["RambaseClientSecret"].ToString();
        private string BaseApiUrl = ConfigurationManager.AppSettings["RambaseBaseUrl"].ToString();
        private string _accesstoken = string.Empty;
        public string _hldId { get; set; }
        public RambaseLink(string hldId)
        {
            _hldId = hldId;
            _accesstoken = (string)HttpRuntime.Cache["access_token"];
            var exp = HttpRuntime.Cache["access_token_time"];
            LogToFil.Log(_hldId, "Accesstoken from cache: " + _accesstoken + ". Time in cache: " + exp);
            
            try
            {
                if (_accesstoken == null)
                {
                    _accesstoken = GetAccessToken();
                    if (string.IsNullOrEmpty(_accesstoken))
                    {
                        throw new Exception("Failed to get access token from Rambase API");
                    }
                }
                                
            }
            catch(Exception e)
            {
                throw new Exception("Could not log in to Rambase API:" + e.Message);
            }
        }
        public string GetAccessToken()
        {
            string oauthstring = string.Empty;
            HttpWebRequest request = WebRequest.Create(BaseApiUrl + "/oauth2/access_token") as HttpWebRequest;           
            request.Host = "api.rambase.net";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Method = "POST";
           
            using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
            {
                writer.Write("client_id=" + ClientId + "&client_secret=" + ClientSecret + "&grant_type=client_credentials");
            }
            try
            {
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    oauthstring = reader.ReadToEnd();
                    if( !string.IsNullOrEmpty(oauthstring))
                    {
                        JObject oauth = JObject.Parse(oauthstring);
                        if (oauth != null)
                        {
                            int exp = 1200;
                            string expiresin = (string)oauth["expires_in"];
                            int.TryParse(expiresin, out exp);
                            exp -= 120;
                            
                            HttpRuntime.Cache.Add("access_token", (string)oauth["access_token"], null, DateTime.Now.AddSeconds(exp), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
                            HttpRuntime.Cache.Add("access_token_time", DateTime.Now.ToString() + TimeSpan.FromSeconds(exp).ToString(), null, DateTime.Now.AddSeconds(exp), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
                            LogToFil.Log(_hldId, "Logged into Rambase API with accesstoken: " + (string)oauth["access_token"] + " and expires_in: " + exp);
                            return (string)oauth["access_token"];
                        }
                    }
                }
            }
            catch (WebException e)
            {
                String serverName = HttpContext.Current.Request.ServerVariables["HTTP_REFERER"];
                String serverName1 = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

                LogToFil.Log("ApiLoginError", serverName + ";" + serverName1 + "\r\n" + e.Message + "\r\n" + e.Response);
                
                throw new Exception("Failed to login to Rambase API: " + e.Message +"\r\n" + e.Response);
                
            }
            return "";

        }
        private string FormatJson(string body)
        {
            if (string.IsNullOrEmpty(body))
                return body;

            JObject obj = JObject.Parse(body);
            return obj.ToString(Formatting.Indented);

        }

        public ApiResponse RunQuery(string resource, string argument = null, object data = null, APIResourceVerb method = APIResourceVerb.GET, bool isJason = false, int retries = 1)
        {
            ApiResponse result = null;
            if (resource.StartsWith("/"))
                resource = resource.Remove(0, 1);
            string url = string.Format("{0}/{1}", BaseApiUrl, resource);
            if (argument != null)
            {
                url = string.Format("{0}/{1}", url, argument);
            }
            
            WebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.ContentType = "application/json";
            request.Method = method.ToString();
            request.Headers.Add("Authorization", "Bearer " + _accesstoken);
            string jsondata = string.Empty;
            if (data != null)
            {
                if (!isJason)
                    jsondata = JsonConvert.SerializeObject(data, Formatting.Indented, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
                else
                    jsondata = data.ToString();
            }
            if (jsondata == "null")
                jsondata = null;

            if (!string.IsNullOrEmpty(_hldId))
                LogToFil.Log(_hldId, "Rambase API: " + method.ToString() + " " + url + "\r\n" + FormatJson(jsondata));
            
            if (data != null)
            {
                using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(jsondata);
                }
            }            
            try
            {
                WebResponse response = request.GetResponse() as WebResponse;                
                result = new ApiResponse(response);
                if (!string.IsNullOrEmpty(result.Result))
                    LogToFil.Log(_hldId, "Rambase API response:\r\n " + FormatJson(result.Result));

            }
            catch (WebException e)
            {
                result = new ApiResponse(e.Response);
                result.ErrorMessage = e.Message;
                result.HasErrors = true;
                LogToFil.Log(_hldId, "Rambase API exception (retry:" + retries + "): " + e.Message + "\r\n" + FormatJson(result.Result));

                //if Unauthorized, try to log in and try again
                if( e.Message.ToLower().Contains("unauthorized") && retries == 1)
                {
                    LogToFil.Log(_hldId, "Rambase API: refresh accesstoken: " + _accesstoken);
                    _accesstoken = GetAccessToken();
                    LogToFil.Log(_hldId, "Rambase API: new accesstoken: " + _accesstoken);
                    RunQuery(resource, argument, data, method, isJason, 2);
                }
            }
            return result;
         
        }
            
    }


}