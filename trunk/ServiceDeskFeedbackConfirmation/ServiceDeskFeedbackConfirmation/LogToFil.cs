﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using System.Web.Caching;

namespace ServiceDeskFeedbackConfirmation
{
    public static class LogToFil
    {
        public static void Log(string HldNo, string body)
        {
            if (string.IsNullOrEmpty(HldNo))
                return;

            if( ConfigurationManager.AppSettings["LogTransactions"].ToString().ToLower() == "true" )
            {           
                string file = ConfigurationManager.AppSettings["LogPath"].ToString() + "\\HLD" + HldNo + ".log";
                LogWriter.WriteLog(file, body);
            }              
            
        }

        public static void DeleteLogFiles()
        {
            string[] files = System.IO.Directory.GetFiles(ConfigurationManager.AppSettings["LogPath"].ToString(), "*.log");
            List<FileInfo> fileList = new List<FileInfo>();
            foreach (string file in files)
            {
                FileInfo fileinfo = new FileInfo(file);
                if (fileinfo.LastAccessTime < DateTime.Now.Subtract(TimeSpan.FromDays(100)))
                    System.IO.File.Delete(fileinfo.FullName);
                fileList.Add(fileinfo);
            }


        }
    }
}