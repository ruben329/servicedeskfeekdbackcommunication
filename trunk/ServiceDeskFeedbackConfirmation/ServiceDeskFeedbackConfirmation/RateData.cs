﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceDeskFeedbackConfirmation
{
    public class RateData
    {
        public SupportRating SupportRating;
        public RateData(int score)
        {
            SupportRating = new SupportRating();
            SupportRating.Rating = score;

        }
    }

    public class SupportRating
    {
        public int Rating { get; set; } //The rating from 1 to 3
    }

}